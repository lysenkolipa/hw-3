# HW-3

Базовая часть

Задача 1.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). 	
Задача 1.2: Создайте объект класса Employee, затем установите его свойства в следующие значения - имя 'Иван', возраст 25, зарплата 1000. 	
Задача 1.3: Создайте второй объект класса Employee, установите его свойства в следующие значения - имя 'Вася', возраст 26, зарплата 2000. 	
Задача 1.4: Выведите на экран сумму зарплат Ивана и Васи. 
Задача 1.5: Выведите на экран сумму возрастов Ивана и Васи.

Задача 2.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). 	
Задача 2.2: Сделайте в классе Employee метод getName, который будет возвращать имя работника. 	
Задача 2.3: Сделайте в классе Employee метод getAge, который будет возвращать возраст работника.
Задача 2.4: Сделайте в классе Employee метод getSalary, который будет возвращать зарплату работника. 	
Задача 2.5: Сделайте в классе Employee метод checkAge, который будет проверять то, что работнику больше 18 лет и возвращать true, если это так, и false, если это не так. 	
Задача 2.6: Создайте два объекта класса Employee, запишите в их свойства какие-либо значения. С помощью метода getSalary найдите сумму зарплат созданных работников.
 	
Задача 3.1: Сделайте класс User, в котором будут следующие свойства - name (имя), age (возраст).	
Задача 3.2: Сделайте метод setAge, который параметром будет принимать новый возраст пользователя.
Задача 3.3: Создайте объект класса User с именем 'Коля' и возрастом 25. С помощью метода setAge поменяйте возраст на 30. Выведите новое значение возраста на экран.
Задача 3.4: Модифицируйте метод setAge так, чтобы он вначале проверял, что переданный возраст больше или равен 18. Если это так - пусть метод меняет возраст пользователя, а если не так - то ничего не делает. 	

Задача 4.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), salary (зарплата). Сделайте метод doubleSalary, который текущую зарплату будет увеличивать в 2 раза. 	

Задача 5.1: Сделайте класс Rectangle (прямоугольник), в котором в свойствах будут записаны ширина и высота (на английском). 
Задача 5.2: В классе Rectangle сделайте метод getSquare, который будет возвращать площадь этого прямоугольника.
Задача 5.3: В классе Rectangle сделайте метод getPerimeter, который будет возвращать периметр этого прямоугольника.

Задача 6.1: Сделайте класс User, в котором будут следующие свойства - name (имя), age (возраст).	
Задача 6.2: Сделайте метод setAge, который параметром будет принимать новый возраст пользователя.
Задача 6.3: Сделайте метод addAge, который параметром будет принимать к-тво лет, которые нужно добавить к возрасту пользователя.
Задача 6.4: Добавьте также метод subAge, который будет выполнять уменьшение текущего возраста на переданное количество лет.
Задача 6.5: Сделайте метод, который будет валидировать возраст, название и сам функционал придумайте сами, исходя из опыт написания валидаторов.
Задача 6.6: Этот метод-валидатор должен вызываться для проверки каждый раз, когда в объект записываются новые данные по возрасту.
Задача 6.7: Создайте объект этого класса User, проверьте работу методов setAge, addAge и subAge.Числа и примеры придумайте сами. Не забудьте за валидацию возраста.

Задача 7.1 (на приватные и публичные свойства и методы): смените модификатор доступа для вашего валидатора из пункта 6.5 в классе User. Теперь он должен быть не public, а private. 	
Задача 7.2: Попробуйте вызвать этот метод для валидации возраста снаружи класса. Убедитесь, что это будет вызывать ошибку.

Задача 8.1: Сделайте класс Student со свойствами $name и $course (курс студента, от 1-го до 5-го). 	
Задача 8.2: В классе Student сделайте public метод transferToNextCourse, который будет переводить студента на следующий курс. 	
Задача 8.3: Выполните в методе transferToNextCourse проверку на то, что следующий курс не больше 5. 	
Задача 8.4: Вынесите проверку курса в отдельный private метод isCourseCorrect. 
Задача 8.5: Сделайте в классе Student приватное свойство $courseAdministrator.
Задача 8.6: Попробуйте записать в это свойство любое имя снаружи класса. Убедитесь, что это будет вызывать ошибку.
Задача 8.7: Чтобы была возможность записать это имя, создайте публичный метод (называется сеттер) setCourseAdministrator, который будет принимать аргументом имя администратора и записывать его в приватное свойство  $courseAdministrator.
Задача 8.8: Чтобы была возможность прочитать это имя, создайте публичный метод (называется геттер) getCourseAdministrator, который будет возвращать приватное свойство  $courseAdministrator.

Задача 9.1: Сделайте класс Employee, в котором будут следующие private свойства: name (имя), age (возраст) и salary (зарплата). 	
Задача 9.2: Сделайте геттеры и сеттеры для всех свойств класса Employee. 	
Задача 9.3: Дополните класс Employee приватным методом isAgeCorrect, который будет проверять возраст на корректность (от 1 до 100 лет). Этот метод должен использоваться в сеттере setAge перед установкой нового возраста 	(если возраст не корректный - он не должен меняться). 	
Задача 9.4: Пусть зарплата наших работников хранится в долларах. Сделайте так, чтобы 	геттер getSalary добавлял в конец числа с зарплатой значок доллара. То есть еще раз для полной ясности условия задачи:в свойстве salary зарплата будет хранится просто числом, но геттер будет возвращать ее с долларом на конце.

Задача 10.1: Сделайте класс Employee, в котором будут следующие свойства: name (имя), surname (фамилия) и salary (зарплата). 	
Задача 10.2: Сделайте так, чтобы свойства name и surname были доступны только для чтения, а свойство salary - и для чтения, и для записи. 	

Задача 11.1: Сделайте класс Employee, в котором будут следующие публичные свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись в методе __construct при создании объекта.	
Задача 11.2: Создайте объект класса Employee с именем 'Вася', возрастом 25, зарплатой 1000.
Задача 11.3: Создайте объект класса Employee с именем 'Петя', возрастом 30, зарплатой 2000. 
Задача 11.4: Выведите на экран сумму зарплат Васи и Пети.



Задача 12.1: Сделайте класс City (город), в котором будут следующие свойства - name (название), foundation (дата основания), population (население). Создайте объект этого класса.
Задача 12.2: Пусть дана переменная $props, в которой хранится массив названий свойств класса City. Переберите этот массив циклом foreach и выведите на экран значения соответствующих свойств. 	

Задача 13.1: Есть класс из задачи 2.
Задача 13.2: Есть ассоциативный массив $methods с ключами method1 и method2: 
<?php 	$methods = ['method1' => 'getName', 'method2' => 'getAge'];
Задача 13.3: Выведите имя и возраст пользователя с помощью этого массива.
Задача 13.4: Отдельно вызовите метод getAge сразу после создания объекта.

Задача 14.1: Сделайте класс User, у которого будут приватные свойства surname (фамилия), name (имя) и patronymic (отчество).
Задача 14.2: Эти свойства должны задаваться с помощью соответствующих сеттеров.
Задача 14.3: Сделайте так, чтобы эти сеттеры вызывались цепочкой в любом порядке, а самым последним методом в цепочке можно было вызвать метод getFullName, который вернет ФИО юзера (первую букву фамилии, имени и отчества). 
Задача 14.4: Создайте объект и сразу же вызовите у него по цепочке все методы. Повторите в разной последовательности.

Задача 15.1: Сделайте класс Product (товар), в котором будут приватные свойства name (название товара), price (цена за штуку) и quantity. Пусть все эти свойства будут доступны только для чтения. 	
Задача 15.2: Добавьте в класс Product метод getCost, который будет находить полную стоимость продукта (сумма умножить на количество). 	
Задача 15.3: Сделайте класс Cart (корзина). Данный класс будет хранить список продуктов (объектов класса Product) в виде массива. Пусть продукты хранятся в свойстве products. 	
Задача 15.4: Реализуйте в классе Cart метод add для добавления продуктов. 
Задача 15.5: Реализуйте в классе Cart метод remove для удаления продуктов. Метод должен принимать параметром название удаляемого продукта. 	
Задача 15.6: Реализуйте в классе Cart метод getTotalCost, который будет 		находить суммарную стоимость продуктов. 	
Задача 15.7: Реализуйте в классе Cart метод getTotalQuantity, который будет находить суммарное количество продуктов (то есть сумму свойств quantity всех продуктов). 	
Задача 15.8: Реализуйте в классе Cart метод getAvgPrice, который будет находить среднюю стоимость продуктов (суммарная стоимость делить на количество всех продуктов).

Задача 16.1: Сделайте класс Employee с публичными свойствами name (имя) и salary (зарплата). 	
Задача 16.2: Сделайте класс Student с публичными свойствами name (имя) и scholarship (стипендия). 	
Задача 16.3: Создайте по 3 объекта каждого класса и в произвольном порядке запишите их в массив $arr. 	
Задача 16.4: Переберите циклом массив $arr и выведите на экран столбец имен всех работников. 	
Задача 16.5: Аналогичным образом выведите на экран столбец имен всех студентов. 
Задача 16.6: Переберите циклом массив $arr и с его помощью найдите сумму зарплат работников и сумму стипендий студентов После цикла выведите эти два числа на экран. 	
