<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

use lysenkolipa\hw3\task1\employee\Employee as Employee1;
use lysenkolipa\hw3\task2\employee\Employee as Employee2;
use lysenkolipa\hw3\task3\user\User;
use lysenkolipa\hw3\task4\employee\Employee as Employee4;
use lysenkolipa\hw3\task5\rectangle\Rectangle;
use lysenkolipa\hw3\task6\user\User as User6;
use lysenkolipa\hw3\task8\student\Student;
use lysenkolipa\hw3\task11\employee\Employee as Employee11;
use lysenkolipa\hw3\task12\city\City;
use lysenkolipa\hw3\task14\user\User as User14;
use lysenkolipa\hw3\task15\product\Product;
use lysenkolipa\hw3\task15\cart\Cart;
use lysenkolipa\hw3\task16\employee\Employee as Employee16;
use lysenkolipa\hw3\task16\student\Student as Student16;


/** Задача 1.2: Создайте объект класса Employee, затем установите его свойства в следующие значения - имя 'Иван', возраст 25, зарплата 1000.
 * Задача 1.3: Создайте второй объект класса Employee, установите его свойства в следующие значения - имя 'Вася', возраст 26, зарплата 2000.
 * Задача 1.4: Выведите на экран сумму зарплат Ивана и Васи.
 * Задача 1.5: Выведите на экран сумму возрастов Ивана и Васи.*/

$employee1 = new Employee1();
$employee1->name = 'Ivan';
$employee1->age = 25;
$employee1->salary = 1000;

$employee2 = new Employee1();
$employee2->name = 'Vasya';
$employee2->age = 26;
$employee2->salary = 2000;
$employeeSalarySum = $employee1->salary + $employee2->salary;
$employeeAgeSum = $employee1->age + $employee2->age;

echo "Task #1:<br>";
echo "Employee salary sum: $employeeSalarySum";
echo '<br>';
echo "Employee age sum: $employeeAgeSum";
echo "<br>----------------------";

/** Задача 2.6: Создайте два объекта класса Employee, запишите в их свойства какие-либо значения.
 * С помощью метода getSalary найдите сумму зарплат созданных работников.
 */

$employee3 = new Employee2();
$employee3->name = 'Petya';
$employee3->age = 18;
$employee3->salary = 600;

$employee4 = new Employee2();
$employee4->name = 'Nina';
$employee4->age = 31;
$employee4->salary = 2700;

$salarySum = $employee3->getSalary() + $employee4->getSalary();

echo "<br>Task #2:<br>";
echo "Employee salary sum: $salarySum";
echo "<br>----------------------";


/*Задача 3.3: Создайте объект класса User с именем 'Коля' и возрастом 25. С помощью метода setAge поменяйте возраст на 30.
Выведите новое значение возраста на экран.*/

echo "<br>Task #3:<br>";
$user0 = new User('Коля', 25);

print_r($user0);
echo "<pre>";

$user0->setAge(30);
print_r($user0);

echo "<pre>Add age: ";
echo $user0->addAge(12);
echo "<pre>Sub age: ";
echo $user0->subAge(8);
echo "<br>----------------------";

/*Задача 4.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), salary (зарплата).
Сделайте метод doubleSalary, который текущую зарплату будет увеличивать в 2 раза. */

$employeeDouble = new Employee4('Test', 2300);
$doubleSalary = $employeeDouble->doubleSalary();

echo "<br>Task #4:<br>";
echo $employeeDouble->getSalary();
echo "<pre>";
echo "Double Salary: $doubleSalary";
echo "<br>----------------------";

/*  Задача 5 */

$rec = new Rectangle(8, 14);
$perimetr = $rec->getPerimeter();
$square = $rec->getSquare();

echo "<br>Task #5:<br>";
echo "width: ";
echo $rec->getWidth();
echo "<br>";
echo "height: ";
echo $rec->getHeight();
echo "<br>";

echo "Perimetr: $perimetr";
echo "<br>";
echo "Square: $square";
echo "<br>----------------------";

/** Задача 6.7: Создайте объект этого класса User, проверьте работу методов setAge, addAge и subAge.
 * Числа и примеры придумайте сами. Не забудьте за валидацию возраста.
 */
$user1 = new User6('Kolya', 25);
$user1->setAge(30);
$addAge = $user1->addAge(2);
$subAge = $user1->subAge(4);


echo "<br>Task #6:<br>";
echo "Add Age: $addAge";
echo '<br>';
echo "Sub Age: $subAge";
echo "<br>----------------------";

/** Задача 7.2: Попробуйте вызвать этот метод для валидации возраста снаружи класса. Убедитесь, что это будет вызывать ошибку. */
//$user->ageValidator();//Method not found!

/** Задача 8.6: Попробуйте записать в это свойство любое имя снаружи класса. Убедитесь, что это будет вызывать ошибку.*/
$student1 = new Student('Vasyl', 2);

//$student->$courseAdministrator = 'test'; //Undefined variable
$student1->setCourseAdministrator('test');

echo "<br>Task #8:<br>";
echo "Course Administrator: ";
echo $student1->getCourseAdministrator();
echo "<br>----------------------";

/**
 * Задача 11.2: Создайте объект класса Employee с именем 'Вася', возрастом 25, зарплатой 1000.
 * Задача 11.3: Создайте объект класса Employee с именем 'Петя', возрастом 30, зарплатой 2000.
 * Задача 11.4: Выведите на экран сумму зарплат Васи и Пети.*/
$employeeNew = new Employee11('Вася', 25, 1000);
$employeeNew1 = new Employee11('Петя', 30, 2000);

echo "<br>Task #11:<br>";
echo "Salary sum: ";
echo $employeeNew->getSalary() + $employeeNew1->getSalary();
echo "<br>----------------------";

/**
 * Задача 12.1: Сделайте класс City (город), в котором будут следующие свойства - name (название), foundation (дата основания),
 * population (население). Создайте объект этого класса.
 * Задача 12.2: Пусть дана переменная $props, в которой хранится массив названий свойств класса City.
 * Переберите этот массив циклом foreach и выведите на экран значения соответствующих свойств.
 */
$city = new City();
$props = [
    $city->name = 'Сумы',
    $city->foundation = 1652,
    $city->population = 268409,
];

print_r($props);

echo "<br>Task #12:<br>";
foreach ($props as $value) {
    echo $value . '<br>';
}
echo "----------------------";

/**
 * Задача 13.1: Есть класс из задачи 2.
 * Задача 13.2: Есть ассоциативный массив $methods с ключами method1 и method2:
 * <?php    $methods = ['method1' => 'getName', 'method2' => 'getAge'];
 * Задача 13.3: Выведите имя и возраст пользователя с помощью этого массива.
 * Задача 13.4: Отдельно вызовите метод getAge сразу после создания объекта.
 */

$methods = ['method1' => 'getName', 'method2' => 'getAge'];

echo "<br>Task #13:<br>";

foreach ($methods as $key => $value) {
    echo $employee3->$value();
}

echo "<br>----------------------";

$employeeFromTask2 = new Employee2();
$employeeFromTask2->getAge();

/**
 * Задача 14.3: Сделайте так, чтобы эти сеттеры вызывались цепочкой в любом порядке, а самым последним методом в цепочке
 * можно было вызвать метод getFullName, который вернет ФИО юзера (первую букву фамилии, имени и отчества).
 * Задача 14.4: Создайте объект и сразу же вызовите у него по цепочке все методы. Повторите в разной последовательности.Задача 14.4: Создайте объект и сразу же вызовите у него по цепочке все методы. Повторите в разной последовательности.
 */
echo "<br>Task #14:<br>";

$user14 = new User14();
$user14->setName('Ivan')->setSurname('Ivanov')->setPatronymic('Ivanovych')->getFullName();


echo $user14->getSurname();
echo "<br>";
echo $user14->getName();
echo "<br>";
echo $user14->getPatronymic();
echo "<br>___________: ";
echo $user14->getFullName();
echo "<br>";

$user14->setSurname('Tamara')->setName('Kochut')->setPatronymic('Stepenovna')->getFullName();

echo $user14->getSurname();
echo "<br>";
echo $user14->getName();
echo "<br>";
echo $user14->getPatronymic();
echo "<br>___________: ";
echo $user14->getFullName();
echo "<br>";

$user14->setPatronymic('Robert')->setSurname('Jabo')->setName('Robert')->getFullName();

echo $user14->getSurname();
echo "<br>";
echo $user14->getName();
echo "<br>";
echo $user14->getPatronymic();
echo "<br>___________: ";
echo $user14->getFullName();
echo "<br>----------------------";


$product = new Product('laptop', 1500, 1);
$product1 = new Product('milk', 23, 2);
$product2 = new Product('flowers', 30, 13);
$cart = new Cart();
$cart->add($product);
$cart->add($product1);
$cart->add($product2);
$cart->remove('milk');
$totalCost = $cart->getTotalCost();
$totalQuantity = $cart->getTotalQuantity();
$avgPrice = $cart->getAvgPrice();

echo "<br>Task #15:";
echo "<br> Shopping cart total cost: $totalCost";
echo "<br> Shopping cart total quantity: $totalQuantity";
echo "<br> Shopping cart average price: $avgPrice<br>";
echo "----------------------";


/**
 * Задача 16.3: Создайте по 3 объекта каждого класса и в произвольном порядке запишите их в массив $arr.
 * Задача 16.4: Переберите циклом массив $arr и выведите на экран столбец имен всех работников.
 * Задача 16.5: Аналогичным образом выведите на экран столбец имен всех студентов.
 * Задача 16.6: Переберите циклом массив $arr и с его помощью найдите сумму зарплат работников и сумму стипендий студентов
 * После цикла выведите эти два числа на экран. */

$employee6 = new Employee16('Tamara', 500);
$employee7 = new Employee16('Adam', 1500);
$employee8 = new Employee16('Henry', 2300);
$student2 = new Student16('Timur', 1000);
$student3 = new Student16('Anna', 1100);
$student4 = new Student16('Arny', 1002);

$arr = [$employee6, $employee7, $employee8, $student2, $student3, $student4];

echo "<br>Task #16:<br>";
echo "Employees name:";

foreach ($arr as $key => $value) {
    if ($value instanceof Employee16) {
        echo "<br>$value->name";
    }
}

echo "<br><br>Students name: ";

foreach ($arr as $key => $value) {
    if ($value instanceof Student16) {
        echo "<br>$value->name";
    }
}

$employeeSalary = 0;
$StudentScholarship = 0;
foreach ($arr as $key => $value) {
    if ($value instanceof Employee16) {
        $employeeSalary += $value->salary;
    }
    if ($value instanceof Student16) {
        $StudentScholarship += $value->scholarship;
    }
}
echo "<br><br> Salary SUM";
echo "<br>Employee: $employeeSalary";
echo "<br>Students: $StudentScholarship";
echo "<br>----------------------";




