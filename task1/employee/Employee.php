<?php

namespace lysenkolipa\hw3\task1\employee;

/** Задача 1.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), age (возраст),
 * salary (зарплата). */
class Employee
{
    public $name;
    public $age;
    public $salary;
}

