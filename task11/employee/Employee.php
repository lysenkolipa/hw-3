<?php

namespace lysenkolipa\hw3\task11\employee;

/**
 * Задача 11.1: Сделайте класс Employee, в котором будут следующие публичные свойства - name (имя), age (возраст), salary (зарплата).
 * Сделайте так, чтобы эти свойства заполнялись в методе __construct при создании объекта.
 */
class Employee
{
    private string $name;
    private int $age;
    private float $salary;

    /**
     * Employee constructor.
     * @param string $name
     * @param int $age
     * @param float $salary
     */
    public function __construct(string $name, int $age, float $salary)
    {
        $this->name = $name;
        $this->age = $age;
        $this->salary = $salary;
    }

    /**
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }
}


