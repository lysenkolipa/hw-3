<?php

namespace lysenkolipa\hw3\task14\user;

/**
 * Задача 14.1: Сделайте класс User, у которого будут приватные свойства surname (фамилия), name (имя) и patronymic (отчество).
 * Задача 14.2: Эти свойства должны задаваться с помощью соответствующих сеттеров.
 * Задача 14.3: Сделайте так, чтобы эти сеттеры вызывались цепочкой в любом порядке, а самым последним методом в цепочке
 * можно было вызвать метод getFullName, который вернет ФИО юзера (первую букву фамилии, имени и отчества).
 * Задача 14.4: Создайте объект и сразу же вызовите у него по цепочке все методы. Повторите в разной последовательности.
 */
class User
{
    private $surname;
    private $name;
    private $patronymic;

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param mixed $patronymic
     * @return User
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    /**
     * @param mixed $surname
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getPatronymic(): string
    {
        return $this->patronymic;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return substr($this->getSurname(), 0, 1) . substr($this->getName(), 0, 1) . substr($this->getPatronymic(), 0, 1);
    }
}

