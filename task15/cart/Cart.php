<?php

namespace lysenkolipa\hw3\task15\cart;

/**
 * Задача 15.3: Сделайте класс Cart (корзина). Данный класс будет хранить список продуктов (объектов класса Product) в виде
 * массива. Пусть продукты хранятся в свойстве products.
 */
class Cart
{
    private array $products;

    /* Задача 15.4: Реализуйте в классе Cart метод add для добавления продуктов.*/

    /**
     * @param $product
     * @return mixed
     */
    public function add($product)
    {
        return $this->products[] = $product;
    }

    /* Задача 15.5: Реализуйте в классе Cart метод remove для удаления продуктов.
        Метод должен принимать параметром название удаляемого продукта. */
    /**
     * @param $productName
     * @return array
     */
    public function remove($productName): array
    {
        foreach ($this->products as $key => $value) {
            if ($value->getName() === $productName) {
                unset($this->products[$key]);
            }
        }
        return $this->products;
    }

    /* Задача 15.6: Реализуйте в классе Cart метод getTotalCost, который будет находить суммарную стоимость продуктов. */

    /**
     * @return int
     */
    public function getTotalCost()
    {
        $totalCost = 0;
        foreach ($this->products as $key => $value) {
            $totalCost += $value->getPrice();
        }
        return $totalCost;
    }

    /* Задача 15.7: Реализуйте в классе Cart метод getTotalQuantity, который будет находить суммарное количество продуктов
    (то есть сумму свойств quantity всех продуктов). */

    /**
     * @return int
     */
    public function getTotalQuantity()
    {
        $totalQuantity = 0;
        foreach ($this->products as $key => $value) {
            $totalQuantity += $value->getQuantity();
        }
        return $totalQuantity;
    }

    /*  Задача 15.8: Реализуйте в классе Cart метод getAvgPrice, который будет находить среднюю стоимость продуктов
    (суммарная стоимость делить на количество всех продуктов).*/

    /**
     * @return float|int
     */
    public function getAvgPrice()
    {
        $avgPrice = $this->getTotalCost() / $this->getTotalQuantity();

        return $avgPrice;
    }
}


