<?php

namespace lysenkolipa\hw3\task15\product;

/**
 * Задача 15.1: Сделайте класс Product (товар), в котором будут приватные свойства name (название товара),
 * price (цена за штуку) и quantity.
 * Пусть все эти свойства будут доступны только для чтения.*/
class Product
{
    private string $name;
    private float $price;
    private int $quantity;

    /**
     * Product constructor.
     * @param $name
     * @param $price
     * @param $quantity
     */
    public function __construct($name, $price, $quantity)
    {
        $this->name = $name;
        $this->price = $price;
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /* Задача 15.2: Добавьте в класс Product метод getCost, который будет находить полную стоимость продукта
    (сумма умножить на количество).*/


    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->getPrice() * $this->getQuantity();
    }
}

