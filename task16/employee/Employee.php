<?php

namespace lysenkolipa\hw3\task16\employee;

/*  Задача 16.1: Сделайте класс Employee с публичными свойствами name (имя) и salary (зарплата). */

class Employee
{
    public string $name;
    public float $salary;

    /**
     * Employee constructor.
     * @param $name
     * @param $salary
     */
    public function __construct($name, $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }
}

