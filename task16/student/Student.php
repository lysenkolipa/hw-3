<?php

namespace lysenkolipa\hw3\task16\student;

/** Задача 16.2: Сделайте класс Student с публичными свойствами name (имя) и scholarship (стипендия).*/
class Student
{
    public string $name;
    public float $scholarship;

    /**
     * Student constructor.
     * @param $name
     * @param $scholarship
     */
    public function __construct($name, $scholarship)
    {
        $this->name = $name;
        $this->scholarship = $scholarship;
    }
}

