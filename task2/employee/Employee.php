<?php

namespace lysenkolipa\hw3\task2\employee;

/** Задача 2.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). */
class Employee
{
    private const AGE = 18;

    public $name;
    public $age;
    public $salary;


    /*  Задача 2.2: Сделайте в классе Employee метод getName, который будет возвращать имя работника.*/

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /*  Задача 2.3: Сделайте в классе Employee метод getAge, который будет возвращать возраст работника. */

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /*  Задача 2.4: Сделайте в классе Employee метод getSalary, который будет возвращать зарплату работника. */

    /**
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }

    /** Задача 2.5: Сделайте в классе Employee метод checkAge, который будет проверять то, что работнику больше 18 лет и
     * возвращать true, если это так, и false, если это не так. */

    public function checkAge(): int
    {
        return $this->age > self::AGE;
    }
}

