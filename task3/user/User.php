<?php

namespace lysenkolipa\hw3\task3\user;

/**
 * Задача 3.1: Сделайте класс User, в котором будут следующие свойства - name (имя), age (возраст).
 * Задача 3.4: Модифицируйте метод setAge так, чтобы он вначале проверял, что переданный возраст больше или равен 18.
 * Если это так - пусть метод меняет возраст пользователя, а если не так - то ничего не делает.
 */
class User
{
    public $addAge;
    public $subAge;

    private const AGE = 18;

    private $name;
    private $age;

    /**
     * User constructor.
     * @param $name
     * @param $age
     */
    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    /*Задача 3.2: Сделайте метод setAge, который параметром будет принимать новый возраст пользователя.*/

    /**
     * @param $age
     */
    public function setAge($age)
    {
        if ($this->age >= self::AGE) {
            $this->age = $age;
        }
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param $addAge
     * @return mixed
     */
    public function addAge($addAge)
    {
        return $this->age + $addAge;
    }

    /**
     * @param $subAge
     * @return mixed
     */
    public function subAge($subAge)
    {
        return $this->age - $subAge;
    }
}


