<?php

namespace lysenkolipa\hw3\task4\employee;

/*  Задача 4.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), salary (зарплата).
Сделайте метод doubleSalary, который текущую зарплату будет увеличивать в 2 раза.
*/

class Employee
{
    private const DOUBLE = 2;

    private string $name;
    private float $salary;

    /**
     * Employee constructor.
     * @param $name
     * @param $salary
     */
    public function __construct($name, $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param float $salary
     */
    public function setSalary(float $salary): void
    {
        $this->salary = $salary;
    }

    /**
     * @return float
     */
    public function doubleSalary(): float
    {
        return $this->salary * self::DOUBLE;
    }
}


