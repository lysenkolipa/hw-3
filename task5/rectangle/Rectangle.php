<?php

namespace lysenkolipa\hw3\task5\rectangle;

/*
Задача 5.1: Сделайте класс Rectangle (прямоугольник), в котором в свойствах будут записаны ширина и высота (на английском).
Задача 5.2: В классе Rectangle сделайте метод getSquare, который будет возвращать площадь этого прямоугольника.
Задача 5.3: В классе Rectangle сделайте метод getPerimeter, который будет возвращать периметр этого прямоугольника.*/

class Rectangle
{
    private const NUM = 2;

    private $width;
    private $height;

    /**
     * Rectangle constructor.
     * @param $width
     * @param $height
     */
    public function __construct($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return float|int
     */
    public function getSquare()
    {
        return ($this->width * $this->height);
    }

    /**
     * @return float|int
     */
    public function getPerimeter()
    {
        return ($this->width + $this->height) * self::NUM;
    }
}


