<?php

namespace lysenkolipa\hw3\task6\user;

/**
 * Задача 6.1: Сделайте класс User, в котором будут следующие свойства - name (имя), age (возраст).
 * Задача 6.2: Сделайте метод setAge, который параметром будет принимать новый возраст пользователя.
 * Задача 6.3: Сделайте метод addAge, который параметром будет принимать к-тво лет, которые нужно добавить к возрасту пользователя.
 * Задача 6.4: Добавьте также метод subAge, который будет выполнять уменьшение текущего возраста на переданное количество лет.
 * Задача 6.5: Сделайте метод, который будет валидировать возраст, название и сам функционал придумайте сами, исходя из опыта написания валидаторов.
 * Задача 6.6: Этот метод-валидатор должен вызываться для проверки каждый раз, когда в объект записываются новые данные по возрасту.
 */
class User
{
    private const AGE = 18;

    private $name;
    private $age;

    /**
     * User constructor.
     * @param $name
     * @param $age
     */
    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        if ($this->ageValidator()) {
            $this->age = $age;
        };
    }

    /**
     * @param $addAge
     * @return mixed
     */
    public function addAge($addAge)
    {
        return $this->age + $addAge;
    }

    /**
     * @param $subAge
     * @return mixed
     */
    public function subAge($subAge)
    {
        return $this->age - $subAge;
    }

    /**
     * Задача 7.1 (на приватные и публичные свойства и методы): смените модификатор доступа для вашего валидатора из
     * пункта 6.5 в классе User. Теперь он должен быть не public, а private.
     */

    /**
     * @param $age
     * @return mixed
     */
    private function ageValidator(): bool
    {
        return $this->age > self::AGE ? true : false;
    }
}

