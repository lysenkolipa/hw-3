<?php

namespace lysenkolipa\hw3\task8\student;

/**
 * Задача 8.1: Сделайте класс Student со свойствами $name и $course (курс студента, от 1-го до 5-го).
 * Задача 8.2: В классе Student сделайте public метод transferToNextCourse, который будет переводить студента на следующий курс.
 * Задача 8.3: Выполните в методе transferToNextCourse проверку на то, что следующий курс не больше 5.
 * Задача 8.4: Вынесите проверку курса в отдельный private метод isCourseCorrect.
 * Задача 8.5: Сделайте в классе Student приватное свойство $courseAdministrator.
 * Задача 8.6: Попробуйте записать в это свойство любое имя снаружи класса. Убедитесь, что это будет вызывать ошибку.
 * Задача 8.7: Чтобы была возможность записать это имя, создайте публичный метод (называется сеттер) setCourseAdministrator,
 * который будет принимать аргументом имя администратора и записывать его в приватное свойство  $courseAdministrator.
 * Задача 8.8: Чтобы была возможность прочитать это имя, создайте публичный метод (называется геттер) getCourseAdministrator,
 * который будет возвращать приватное свойство  $courseAdministrator.
 */
class Student
{
    private $name;
    private $course;
    private $courseAdministrator;

    /**
     * Student constructor.
     * @param $name
     * @param $course
     */
    public function __construct($name, $course)
    {
        $this->name = $name;
        $this->course = $course;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $course
     */
    public function setCourse($course)
    {
        $this->course = $course;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @return bool|int
     */
    public function transferToNextCourse()
    {
        return $this->isCourseCorrect() ? $this->course + 1 : false;
    }

    /**
     * @return bool
     */
    private function isCourseCorrect()
    {
        return $this->course >= 1 && $this->course <= 5 ? $this->course : false;
    }

    /**
     * @param $courseAdministrator
     */
    public function setCourseAdministrator($courseAdministrator)
    {
        $this->courseAdministrator = $courseAdministrator;
    }

    /**
     * @return mixed
     */
    public function getCourseAdministrator()
    {
        return $this->courseAdministrator;
    }
}

