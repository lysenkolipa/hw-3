<?php

namespace lysenkolipa\hw3\task9\employee;

/**
 * Задача 9.1: Сделайте класс Employee, в котором будут следующие private свойства: name (имя), age (возраст) и salary (зарплата).
 * Задача 9.4: Пусть зарплата наших работников хранится в долларах. Сделайте так, чтобы    геттер getSalary добавлял в
 * конец числа с зарплатой значок доллара. То есть еще раз для полной ясности условия задачи:в свойстве salary зарплата
 * будет хранится просто числом, но геттер будет возвращать ее с долларом на конце.
 */
class Employee
{
    private const CURRENCY = ' USD';

    private $name;
    private $age;
    private $salary;

    /**
     * Employee constructor.
     * @param $name
     * @param $age
     * @param $salary
     */
    public function __construct($name, $age, $salary)
    {
        $this->name = $name;
        $this->age = $age;
        $this->salary = $salary;
    }

    /*  Задача 9.2: Сделайте геттеры и сеттеры для всех свойств класса Employee.*/
    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        if ($this->isAgeCorrect()) {
            $this->age = $age;
        }
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @param mixed $name
     */
    public function getName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /*  Задача 9.3: Дополните класс Employee приватным методом isAgeCorrect, который будет проверять возраст на корректность
    (от 1 до 100 лет). Этот метод должен использоваться в сеттере setAge перед установкой нового возраста
    (если возраст не корректный - он не должен меняться). */

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary . self::CURRENCY;
    }

    /**
     * @return bool
     */
    private function isAgeCorrect(): bool
    {
        return $this->age > 1 && $this->age < 100 ? true : false;
    }
}


